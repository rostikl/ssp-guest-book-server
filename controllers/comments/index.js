'use strict';

// GET Comments controller.

var MAX_COMMENTS_PER_PAGE = 100;

var IndexCommentsController = function (req, res) {
  var Comment = req.app.models.comment;
  var query = req.query;
  
  var paginationParams = {
    offset: +query.offset || 0,
    limit: Math.min(+query.limit || MAX_COMMENTS_PER_PAGE, MAX_COMMENTS_PER_PAGE),
  };
  
  Comment.paginate({}, paginationParams,
    function (err, result) {
      if (err) {
        return res.status(500).json(err);
      }
      
      res.json(result.docs);
    });
};

module.exports = IndexCommentsController;

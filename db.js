'use strict';

/**
 * Connect and setup MongDB via Mongoose.
 */

var mongoose = require('mongoose');

var connect = function (config) {
  var dbUri = config.uri || config;
  
  // Connect to dabatase.
  mongoose.connect(dbUri, config);
  
  var db = mongoose.connection;
  
  // Handle error.
  db.on('error', console.error.bind(console, 'Mongoose connection error:'));
  
  // Handle connection event.
  db.on('open', console.log.bind(console, 'Mongoose succesfully connected to ' + dbUri));
  
  // Handle 'disconnection' event.
  db.on('disconnected', console.log.bind(console, 'Mongoose default connection disconnected from ' + dbUri));
  
  // If the Node process ends, close the Mongoose connection 
  process.on('SIGINT', function() {  
    db.close(function () { 
      console.log('Mongoose default connection disconnected through app termination'); 
      process.exit(0);
    }); 
  }); 
  
  return db;
};

module.exports = connect;
